package com.alrprojects.manager.domain.enums;

public enum StatusEnum {
    A("Active"), I("Inactive");

    private String label;

    private StatusEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }
}
