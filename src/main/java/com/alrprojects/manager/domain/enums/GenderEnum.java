package com.alrprojects.manager.domain.enums;

public enum GenderEnum {
    M("Male"), F("Female");
    private String label;

    private GenderEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }
}
