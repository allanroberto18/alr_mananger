package com.alrprojects.manager.domain.entity;

import com.alrprojects.manager.domain.enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

import java.util.Set;

import javax.persistence.*;

import org.springframework.hateoas.ResourceSupport;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(name = "person")
public class Person extends ResourceSupport {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @ApiModelProperty(notes = "Unique identifier of the person.", required = true, position = 0)
    private Integer id;

    @Column(name = "name")
    @ApiModelProperty(notes = "Name of the person. ", required = true, position = 1)
    private String name;
    
    @Column(name = "email")
    @ApiModelProperty(notes = "Email of the person. ", required = true, position = 2)
    private String email;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "Status of person. ", example = "A(Active)/I(Inactive)", required = true, position = 3)
    private StatusEnum status;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "personId")
    private Set<Parent> parents;

    public Person() {}

    public Person(Integer id) {
        this.id = id;
    }

    public Person(String name, String email, StatusEnum status) {
        this.name = name;
        this.email = email;
        this.status = status;
    }

    public Person(Integer id, String name, String email, StatusEnum status) {
        this(name, email, status);
        this.id = id;
    }

    @JsonProperty("id")
    public Integer getItemId() {
        return id;
    }

    @JsonProperty("id")
    public void setItemId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
    	return this.email;
    }
    
    public void setEmail(String email) {
    	this.email = email;
    }   

	public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

	public Set<Parent> getParents() {
		return parents;
	}

	public void setParents(Set<Parent> parents) {
		this.parents = parents;
	}
}
