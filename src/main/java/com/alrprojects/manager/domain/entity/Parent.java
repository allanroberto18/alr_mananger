package com.alrprojects.manager.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.hateoas.ResourceSupport;

import com.alrprojects.manager.domain.enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(name = "parent")
public class Parent extends ResourceSupport {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @ApiModelProperty(notes = "Unique identifier of the person.", required = true, position = 0)
    private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "person_id")
	@JsonBackReference
	private Person personId;

    @Column(name = "name")
    @ApiModelProperty(notes = "Parent name. ", required = true, position = 1)
    private String name;
    
    @Column(name = "email")
    @ApiModelProperty(notes = "Parent email. ", required = true, position = 2)
    private String email;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "Status of person. ", example = "A(Active)/I(Inactive)", required = true, position = 3)
    private StatusEnum status;

	public Parent() { }

	public Parent(Integer id) {
		this.id = id;
	}

	public Parent(String name, String email, StatusEnum status) {
		this.name = name;
		this.email = email;
		this.status = status;
	}

	public Parent(Integer id, String name, String email, StatusEnum status) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.status = status;
	}

	@JsonProperty("id")
	public Integer getItemId() {
		return id;
	}

	@JsonProperty("id")
	public void setItemId(Integer id) {
		this.id = id;
	}

	public Person getPersonId() {
		return personId;
	}

	public void setPersonId(Person personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}
}
