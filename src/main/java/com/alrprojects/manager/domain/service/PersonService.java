package com.alrprojects.manager.domain.service;

import com.alrprojects.manager.domain.entity.Person;
import com.alrprojects.manager.infrastructure.dto.PersonRequestDTO;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PersonService {

    Optional<Person> findById(PersonRequestDTO dto);

    Person save(PersonRequestDTO dto);

    Page<Person> findAll(Pageable pageable);

    void remove(PersonRequestDTO dto);
}
