package com.alrprojects.manager.domain.service;

import com.alrprojects.manager.domain.entity.Parent;
import com.alrprojects.manager.domain.entity.Person;
import com.alrprojects.manager.infrastructure.dto.ParentRequestDTO;
import com.alrprojects.manager.infrastructure.repository.ParentRepository;
import com.alrprojects.manager.infrastructure.repository.PersonRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ParentServiceImpl implements ParentService {

    @Autowired
    private ParentRepository repository;
    
    @Autowired
    private PersonRepository personRepository;

    public ParentServiceImpl(ParentRepository repository, PersonRepository personRepository) {
		this.repository = repository;
		this.personRepository = personRepository;
	}

	@Override
    public Optional<Parent> findById(ParentRequestDTO dto) {
        Integer id = dto.getId();

        return this.repository.findById(id);
    }

    @Override
    public Parent save(ParentRequestDTO dto) {
    	Person person = this.getPerson(dto);
    	if (person == null) {
    		return null;
    	}
    	
        Parent entity = dto.getEntity();
        entity.setPersonId(person);
        
        if (entity.getItemId() == null) {
            return this.create(entity);
        }

        return this.update(entity);
    }

    private Parent create(Parent entity) {
        return this.repository.save(entity);
    }

    private Parent update(Parent entity) {
        Integer id = entity.getItemId();

        Optional<Parent> opt = this.repository.findById(id);
        if (!opt.isPresent()) {
            entity.setItemId(null);
        }
        
        Parent parent = opt.get();
        parent.setName(entity.getName());
        parent.setEmail(entity.getEmail());
        parent.setStatus(entity.getStatus());
        parent.setPersonId(entity.getPersonId());
        
        return this.repository.save(parent);
    }

    @Override
    public void remove(ParentRequestDTO dto) {
        Optional<Parent> opt = this.repository.findById(dto.getId());

        if (opt.isPresent() == false) {
            return;
        }

        Parent entity = opt.get();

        this.repository.delete(entity);
    }

    @Override
    public Page<Parent> findAll(ParentRequestDTO dto, Pageable pageable) {
    	Person person = this.getPerson(dto);
    	if (person == null) {
    		return null;
    	}
    	
        return this.repository.findAllByPersonId(person, pageable);
    }
    
    private Person getPerson(ParentRequestDTO dto) {
    	if (dto.getPersonId() == null) {
    		return null;
    	}
    	
    	Optional<Person> opt = this.personRepository.findById(dto.getPersonId());
    	if (!opt.isPresent()) {
    		return null;
    	}
    	
    	return opt.get();
    }
    
}
