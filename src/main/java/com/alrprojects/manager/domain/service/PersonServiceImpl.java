package com.alrprojects.manager.domain.service;

import com.alrprojects.manager.domain.entity.Person;
import com.alrprojects.manager.infrastructure.dto.PersonRequestDTO;
import com.alrprojects.manager.infrastructure.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository repository;

    public PersonServiceImpl(PersonRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Person> findById(PersonRequestDTO dto) {
        Integer id = dto.getId();

        return this.repository.findById(id);
    }

    @Override
    public Person save(PersonRequestDTO dto) {
        Person entity = dto.getEntity();
        if (entity.getItemId() == null) {
            return this.create(entity);
        }

        return this.update(entity);
    }

    private Person create(Person entity) {
        return this.repository.save(entity);
    }

    private Person update(Person entity) {
        Integer id = entity.getItemId();

        Optional<Person> opt = this.repository.findById(id);
        if (!opt.isPresent()) {
            entity.setItemId(null);
        }
        
        Person person = opt.get();
        person.setName(entity.getName());
        person.setEmail(entity.getEmail());
        person.setStatus(entity.getStatus());
        
        return this.repository.save(person);
    }

    @Override
    public void remove(PersonRequestDTO dto) {
        Optional<Person> opt = this.repository.findById(dto.getId());

        if (opt.isPresent() == false) {
            return;
        }

        Person entity = opt.get();

        this.repository.delete(entity);
    }

    @Override
    public Page<Person> findAll(Pageable pageable) {
        return this.repository.findAll(pageable);
    }
}
