package com.alrprojects.manager.domain.service;

import com.alrprojects.manager.domain.entity.Parent;
import com.alrprojects.manager.infrastructure.dto.ParentRequestDTO;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ParentService {

    Optional<Parent> findById(ParentRequestDTO dto);

    Parent save(ParentRequestDTO dto);

    Page<Parent> findAll(ParentRequestDTO dto, Pageable pageable);

    void remove(ParentRequestDTO dto);
}
