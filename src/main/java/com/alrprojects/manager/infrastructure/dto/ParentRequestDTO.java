package com.alrprojects.manager.infrastructure.dto;

import com.alrprojects.manager.application.helper.StatusHelper;
import com.alrprojects.manager.domain.entity.Parent;
import com.alrprojects.manager.domain.enums.StatusEnum;

public class ParentRequestDTO {

	private Integer id;
	
	private Integer personId;
	
	private String name;
	
	private String email;
	
	private String status;

	public ParentRequestDTO() { }

	public ParentRequestDTO(String name, String email, String status) {
		this.name = name;
		this.email = email;
		this.status = status;
	}

	public ParentRequestDTO(Integer id, String name, String email, String status) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.status = status;
	}

	/**
	 * @param personId
	 */
	public void fromController(Integer personId) {
		this.personId = personId;
	}

	/**
	 * @param id
	 * @param personId
	 */
	public void fromController(Integer id, Integer personId) {
		this.id = id;
		this.personId = personId;
	}
	
	public Parent getEntity() {
    	String name = this.name;
    	String email = this.email;
    	StatusEnum status = StatusHelper.getStatus(this.status);
    	
    	if (this.id == null) {
    		new Parent(name, email, status);
    	}
        
    	return new Parent(this.id, name, email, status);
    }

	public Integer getId() {
		return id;
	}

	public Integer getPersonId() {
		return personId;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getStatus() {
		return status;
	}
}
