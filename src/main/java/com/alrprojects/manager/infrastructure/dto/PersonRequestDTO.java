package com.alrprojects.manager.infrastructure.dto;

import com.alrprojects.manager.domain.entity.Person;
import com.alrprojects.manager.domain.enums.StatusEnum;
import com.alrprojects.manager.application.helper.StatusHelper;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PersonRequestDTO {

	private Integer id;
	
	@NotBlank(message = "Name can't be blank")
    @NotEmpty(message = "Name can't be empty")
    @Size(min = 4, max = 100, message = "Size of Name have to be greater than 4 and less than 100")
	private String name;
	
	@NotBlank(message = "Email can't be blank")
    @NotEmpty(message = "Email can't be empty")
	@Email(message = "Email invalid")
	private String email;
	
	@NotNull(message = "Status cannot be null")
	private String status;
	
    public PersonRequestDTO() {}
    
    public PersonRequestDTO(String name, String email, String status) {
        this.name = name;
        this.email = email;
        this.status = status;
    }

    public PersonRequestDTO(Integer id, String name, String email, String status) {
        this(name, email, status);
        this.id = id;
    }
    
    /**
     * @param id
     */
    public void fromController(Integer id) {
    	this.id = id;
    }

    public Person getEntity() {
    	String name = this.name;
    	String email = this.email;
    	StatusEnum status = StatusHelper.getStatus(this.status);
    	
    	if (this.id == null) {
    		new Person(name, email, status);
    	}
        
    	return new Person(this.id, name, email, status);
    }

	public Integer getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getEmail() {
		return this.email;
	}

	public String getStatus() {
		return this.status;
	}
}
