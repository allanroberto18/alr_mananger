package com.alrprojects.manager.infrastructure.repository;

import com.alrprojects.manager.domain.entity.Person;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PersonRepository extends PagingAndSortingRepository<Person, Integer> {
    
}
