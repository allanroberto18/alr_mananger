package com.alrprojects.manager.infrastructure.repository;

import com.alrprojects.manager.domain.entity.Parent;
import com.alrprojects.manager.domain.entity.Person;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ParentRepository extends PagingAndSortingRepository<Parent, Integer> {
    Page<Parent> findAllByPersonId(Person personId, Pageable pageable); 
}
