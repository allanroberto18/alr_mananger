package com.alrprojects.manager.infrastructure.resources;

import com.alrprojects.manager.application.action.ParentAction;
import com.alrprojects.manager.domain.entity.Parent;
import com.alrprojects.manager.infrastructure.dto.ParentRequestDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Optional;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RestController
@RequestMapping("/people/{personId}/parents")
@Api("Parent endpoint")
public class ParentResource {

    @Autowired
    private ParentAction action;

    public ParentResource(ParentAction action) {
        this.action = action;
    }

    @GetMapping("/{id}")
    @ApiOperation("Return Parent by id")
    public ResponseEntity<?> findOne(
    		@ApiParam("Person Id") @PathVariable("personId") Integer personId, 
    		@ApiParam("Parent Id") @PathVariable("id") Integer id, 
    		Pageable pageable
		) {
        ParentRequestDTO dto = new ParentRequestDTO();
        
        dto.fromController(id, personId);

        Optional<Parent> opt = this.action.findById(dto);
        if (!opt.isPresent()) {
        	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        
        Parent entity = opt.get();
        
        entity.add(linkTo(methodOn(ParentResource.class).findAll(personId, pageable)).withRel("List"));
        
        return new ResponseEntity<Parent>(opt.get(), HttpStatus.OK);
    }

    @GetMapping("/")
    @ApiOperation("Return list of Parents")
	public ResponseEntity<?> findAll(@ApiParam("Person Id") @PathVariable("personId") Integer personId, Pageable pageable) {
    	ParentRequestDTO dto = new ParentRequestDTO();
    	
    	dto.fromController(personId);
    	
        Page<Parent> list = this.action.findAll(dto, pageable);
        
        for (Parent Parent: list) {
			Integer id = Parent.getItemId();
			
			Parent.add(linkTo(methodOn(ParentResource.class).findOne(personId, id, pageable)).withSelfRel());
		}
        
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(value = "/", produces = "application/json")
    @ApiOperation("Create a new Parent")
    public ResponseEntity<?> create(
    		@ApiParam("Person Id") @PathVariable("personId") Integer personId, 
    		@ApiParam("Parent data(name, email, status)") @Valid @RequestBody ParentRequestDTO dto, Pageable pageable) {
        
    	dto.fromController(personId);

    	Parent entity = this.action.save(dto);
        
        entity.add(linkTo(methodOn(ParentResource.class).findOne(personId, entity.getItemId(), pageable)).withSelfRel());
        
        return new ResponseEntity<Parent>(entity, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", produces = "application/json")
    @ApiOperation("Update Parent Information by Id")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void update(
    		@ApiParam("Person Id") @PathVariable("personId") Integer personId,
    		@ApiParam("Id of Parent") @PathVariable Integer id, 
    		@ApiParam("Parent data(name, gender, dateOfBirth, status)") @Valid @RequestBody ParentRequestDTO dto
		) {
    	
    	dto.fromController(id, personId);
		
    	this.action.save(dto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Delete Parent")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void remove(
    		@ApiParam("Person Id") @PathVariable("personId") Integer personId,
    		@ApiParam("Id of Parent") @PathVariable Integer id
		) {
        
    	ParentRequestDTO dto = new ParentRequestDTO();
        dto.fromController(id, personId);

        this.action.remove(dto);
    }
}
