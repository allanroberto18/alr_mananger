package com.alrprojects.manager.infrastructure.resources;

import com.alrprojects.manager.application.action.PersonAction;
import com.alrprojects.manager.domain.entity.Person;
import com.alrprojects.manager.infrastructure.dto.PersonRequestDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.Optional;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RestController
@RequestMapping("/people")
@Api("Person endpoint")
public class PersonResource {

    @Autowired
    private PersonAction action;

    public PersonResource(PersonAction action) {
        this.action = action;
    }

    @GetMapping("/{id}")
    @ApiOperation("Return Person by id")
    public ResponseEntity<?> findOne(@ApiParam("Id of Person") @PathVariable Integer id, Pageable pageable) {
        PersonRequestDTO dto = new PersonRequestDTO();
        dto.fromController(id);

        Optional<Person> opt = this.action.findById(dto);
        if (!opt.isPresent()) {
        	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        
        Person entity = opt.get();
        
        entity.add(linkTo(methodOn(PersonResource.class).findAll(pageable)).withRel("List"));
        
        return new ResponseEntity<Person>(opt.get(), HttpStatus.OK);
    }

    @GetMapping("/")
    @ApiOperation("Return list of Persons")
	public ResponseEntity<?> findAll(Pageable pageable) {
        Page<Person> list = this.action.findAll(pageable);
        
        for (Person person: list) {
			Integer id = person.getItemId();
			
			person.add(linkTo(methodOn(PersonResource.class).findOne(id, pageable)).withSelfRel());
		}
        
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(value = "/", produces = "application/json")
    @ApiOperation("Create a new Person")
    public ResponseEntity<?> create(@ApiParam("Person data(name, email, status)") @Valid @RequestBody PersonRequestDTO dto, Pageable pageable) {
        Person entity = this.action.save(dto);
        
        entity.add(linkTo(methodOn(PersonResource.class).findOne(entity.getItemId(), pageable)).withSelfRel());
        
        return new ResponseEntity<Person>(entity, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", produces = "application/json")
    @ApiOperation("Update Person Information by Id")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void update(
    		@ApiParam("Id of Person") @PathVariable Integer id, 
    		@ApiParam("Person data(name, email, status)") @Valid @RequestBody PersonRequestDTO dto) {
    	
    	dto.fromController(id);
    	
		this.action.save(dto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Delete Person")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void remove(@ApiParam("Id of Person") @PathVariable Integer id) {
        PersonRequestDTO dto = new PersonRequestDTO();

        dto.fromController(id);
        
        this.action.remove(dto);
    }
}
