package com.alrprojects.manager.application.helper;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeHelper {

    public static Date convertToDate(String param) throws ParseException {
        if (param == null) {
            throw new NullPointerException("The param value is null");

        }

        SimpleDateFormat sDF = new SimpleDateFormat("yyyy-MM-dd");
        TimeZone tZ = TimeZone.getTimeZone("Etc/GMT");
        sDF.setTimeZone(tZ);

        Date dt = sDF.parse(param);

        return dt;
    }
}
