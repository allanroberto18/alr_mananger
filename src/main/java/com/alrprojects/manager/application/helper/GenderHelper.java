package com.alrprojects.manager.application.helper;

import com.alrprojects.manager.domain.enums.GenderEnum;

public class GenderHelper {

    public static GenderEnum getGender(String param) {
        switch (param) {
            case "M":
                return GenderEnum.M;
            case "F":
                return GenderEnum.F;
            default:
                return GenderEnum.M;
        }
    }
}
