package com.alrprojects.manager.application.helper;

import com.alrprojects.manager.domain.enums.StatusEnum;

public class StatusHelper {

    public static StatusEnum getStatus(String param) {
        switch (param) {
            case "A":
                return StatusEnum.A;
            case "I":
                return StatusEnum.I;
            default:
                return StatusEnum.A;
        }
    }
}
