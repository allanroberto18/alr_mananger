package com.alrprojects.manager.application.action;

import com.alrprojects.manager.domain.entity.Person;
import com.alrprojects.manager.domain.service.PersonService;
import com.alrprojects.manager.infrastructure.dto.PersonRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonActionImpl implements PersonAction {

    @Autowired
    private PersonService service;

    public PersonActionImpl(PersonService service) {
        this.service = service;
    }

    @Override
    public Person save(PersonRequestDTO dto) {
        return this.service.save(dto);
    }

    @Override
    public Optional<Person> findById(PersonRequestDTO dto) {
        return this.service.findById(dto);
    }

    @Override
    public Page<Person> findAll(Pageable pageable) {
        return this.service.findAll(pageable);
    }

    @Override
    public void remove(PersonRequestDTO dto) {
        this.service.remove(dto);
    }
}
