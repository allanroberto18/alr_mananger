package com.alrprojects.manager.application.action;

import com.alrprojects.manager.domain.entity.Parent;
import com.alrprojects.manager.domain.service.ParentService;
import com.alrprojects.manager.infrastructure.dto.ParentRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ParentActionImpl implements ParentAction {

    @Autowired
    private ParentService service;

    public ParentActionImpl(ParentService service) {
        this.service = service;
    }

    @Override
    public Parent save(ParentRequestDTO dto) {
        return this.service.save(dto);
    }

    @Override
    public Optional<Parent> findById(ParentRequestDTO dto) {
        return this.service.findById(dto);
    }

    @Override
    public Page<Parent> findAll(ParentRequestDTO dto, Pageable pageable) {
        return this.service.findAll(dto, pageable);
    }

    @Override
    public void remove(ParentRequestDTO dto) {
        this.service.remove(dto);
    }
}
