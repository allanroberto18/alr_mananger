package com.alrprojects.manager.application.action;

import com.alrprojects.manager.domain.entity.Parent;
import com.alrprojects.manager.infrastructure.dto.ParentRequestDTO;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ParentAction {
    Parent save(ParentRequestDTO dto);

    Optional<Parent> findById(ParentRequestDTO dto);

    Page<Parent> findAll(ParentRequestDTO dto, Pageable pageable);

    void remove(ParentRequestDTO dto);
}
