package com.alrprojects.manager.application.action;

import com.alrprojects.manager.domain.entity.Person;
import com.alrprojects.manager.infrastructure.dto.PersonRequestDTO;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PersonAction {
    Person save(PersonRequestDTO dto);

    Optional<Person> findById(PersonRequestDTO dto);

    Page<Person> findAll(Pageable pageable);

    void remove(PersonRequestDTO dto);
}
