CREATE TABLE person (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL, 
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    status CHAR DEFAULT 'A',
	PRIMARY KEY(id)
) Engine = InnoDB;