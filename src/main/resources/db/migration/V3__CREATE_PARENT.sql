CREATE TABLE parent (
	id INT NOT NULL AUTO_INCREMENT,
    person_id INT NOT NULL, 
    name VARCHAR(100) NOT NULL, 
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    stauts CHAR DEFAULT 'A',
	PRIMARY KEY(id)
) Engine = InnoDB;

ALTER TABLE parent ADD CONSTRAINT fk_person_parent FOREIGN KEY (person_id) REFERENCES person (id);