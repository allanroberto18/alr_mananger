create table Person (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    gender VARCHAR(30) NOT NULL,
    dateOfBirth TIMESTAMP NOT NULL,
    status VARCHAR(30) NOT NULL,
    PRIMARY KEY(id)
) Engine=InnoDB;

INSERT INTO Person (name, gender, dateOfBirth, status) VALUES ('Allan Roberto', 'M', '1981-04-18', 'A');
INSERT INTO Person (name, gender, dateOfBirth, status) VALUES ('Jhennifer Teixeira F. Silva', 'F', '1983-06-28', 'A');